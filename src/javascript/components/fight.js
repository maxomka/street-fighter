import { 
  setInitialState, 
  setFighterHealth,
  calcHealthIndicator,
  setStatusFighterCriticalHitBlock
} from '../util/state';

import { 
  keyDownHandler, 
  keyUpHandler 
} from '../util/actions';

import { controls } from '../../constants/controls';
import { changeHealthIndicator } from './arena';

export async function fight(firstFighter, secondFighter) {
  setInitialState(firstFighter, secondFighter);
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    document.onkeydown = keyDownHandler.bind(null, [firstFighter, secondFighter], resolve);
    document.onkeyup = keyUpHandler;
  });
}

export const fighterAction = (attacker, defender, position, isCriticalHit) => {
  let damage;
  if (isCriticalHit) {
    const this_position = position == 'left' ? 'right' : 'left';
    setStatusFighterCriticalHitBlock(this_position, true);
    setTimeout(
      () => setStatusFighterCriticalHitBlock(this_position, false), 10000
    );
    damage = getDamageCriticalHit(attacker);
  } else {
    damage = getDamage(attacker, defender);
  } 
  setFighterHealth(position, damage);
  const newPercent = calcHealthIndicator(defender, position);
  changeHealthIndicator(newPercent, position);
}

export function getDamage(attacker, defender) {
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender); 
  return damage > 0 ? damage : 0;
}

export function getHitPower({attack}) {
  // return hit power
  const criticalHitChance = Math.random() + 1;
  const power = attack * criticalHitChance;
  return power;
}

export function getBlockPower({defense}) {
  // return block power
  const dodgeChance = Math.random() + 1;
  const power = defense * dodgeChance;
  return power;
}

export function getDamageCriticalHit({attack}) {
  const power = 2 * attack;
  return power;
}