import { createElement } from '../helpers/domHelper';
import Health from '../../../resources/details/heart.png';
import Attack from '../../../resources/details/fist.png';
import Defense from '../../../resources/details/shield.png';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  if (fighter) {
    const fighterImg = createFighterImage(fighter);
    const fighterInfo = createFighterPrevInfo(fighter);
    fighterElement.append(fighterImg, fighterInfo);
  } 

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

function createFighterPrevInfo(fighter) {
  const info = createElement({
    tagName: 'div',
    className: 'fighter-preview___info'
  });
  const name = createElement({
    tagName:'h2',
    className: 'fighter-preview___info_name'
  });
  name.innerText = fighter.name;

  const info_obj = {};
  const svg_obj = {
    'health': Health,
    'attack': Attack,
    'defense': Defense,
  };

  Object.entries(fighter).map(([key, value]) => {    
    if (svg_obj[key]) {
      info_obj[key] = createFighterPrevInfoElement(svg_obj[key], value)
    }    
  });
  
  const { health, attack, defense } = info_obj;
  info.append(name, health, attack, defense);
  return info;
}

function createFighterPrevInfoElement(picture, value) {
  const element = createElement({
    tagName: 'div',
    className: 'fighter-preview___info_row'
    
  });
  const wrapElement = createElement({tagName: 'span'});  
  const pictureElement = createElement({tagName: 'img'});
  pictureElement.setAttribute('src', picture);  
  const valueElement = createElement({tagName: 'span'});
  valueElement.innerText = value;
  wrapElement.append(pictureElement);  
  element.append(wrapElement, valueElement);
  return element;
}